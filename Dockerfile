FROM openjdk:latest
VOLUME /tmp
ADD /target/cicd-pipeline-basf-azure-0.0.1-SNAPSHOT.jar app.jar
ENV PORT 80
EXPOSE 80
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dserver.port=80","-jar","/app.jar"]